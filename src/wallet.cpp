using namespace std;
#include <Arduino.h>

#include "Bitcoin.h"
#include <Hash.h>
#include "ESPRandom.h"
#include "ArduinoNvs.h"

#include "wallet.h"
#include "utils.h"

Wallet::Wallet() {
    String m_phrase = "";
}

void Wallet::generatePhrase() {
    delay(50); // delays for good look
    vector<uint8_t> entropy1 = ESPRandom::uuid4();
    delay(77);
    vector<uint8_t> entropy2 = ESPRandom::uuid4();
    delay(42);
    vector<uint8_t> entropy3 = ESPRandom::uuid4();

    vector<uint8_t> entropy;

    entropy.reserve(entropy1.size() + entropy2.size() + entropy3.size()); // preallocate memory
    entropy.insert(entropy.end(),entropy1.begin(),entropy1.end());
    entropy.insert(entropy.end(),entropy2.begin(),entropy2.end());
    entropy.insert(entropy.end(),entropy3.begin(),entropy3.end());

    uint8_t* entropyArr = &entropy[0];

    String phrase = generateMnemonic(entropyArr, entropy.size());

    m_phrase = phrase;

    vector<string> phraseVector = split(string(phrase.c_str())," ");

    m_phraseVector = phraseVector;

    // WiFi is used for entropy for the phrase generation, but is not needed later on
    WiFi.mode(WIFI_OFF);
}

bool Wallet::phraseGenerated() {
    return (m_phrase!="");
}

void Wallet::setPassword(String password) {
    sha256(password, m_passwordHash);
    return;
}

bool Wallet::checkPassword(String password) {
    uint8_t toCheck[32];
    sha256(password, toCheck);
    std::set<int> p1(std::begin(toCheck), std::end(toCheck));
    std::set<int> p2(std::begin(m_passwordHash), std::end(m_passwordHash));
    return p1 == p2;
}

String Wallet::getPhrase() {
    return m_phrase;
}

String Wallet::getPhraseAt(int index) {
    return m_phraseVector.at(index).c_str();
}

void Wallet::unlock(const String password) {
    Serial.println("Unlocking with password: "+password+" and phrase_m "+m_phrase);
    // creating root key from the recovery phrase and some password
    HDPrivateKey root("inherit focus liar catch knee room amateur state situate whip stumble original wealth stage volcano whisper shed fruit soul spice surface neither time ocean", "my super secure password");
    Serial.println(root);
    Serial.println("Object init");
    //m_privatekey = &root;
    Serial.println("Pointer set");
}

void Wallet::lock() {
    delete m_privatekey;
    m_privatekey = nullptr;
}

String Wallet::getPrivateKey() {
    if (m_privatekey!=nullptr) {
        return m_privatekey->xprv();
    } else {
        return "";
    }
}

void Wallet::save() {
    NVS.setString("phrase", m_phrase);
    NVS.setBlob("password", m_passwordHash, sizeof(m_passwordHash));
    NVS.commit();
}

void Wallet::wipe() {
    NVS.setString("phrase", "");
    NVS.setBlob("password", {}, 0);
    NVS.commit();
}

void Wallet::load() {
    m_phrase = NVS.getString("phrase");
    NVS.getBlob("password", m_passwordHash, sizeof(m_passwordHash));
}
