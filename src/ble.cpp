using namespace std;
#include <Arduino.h>

#include <NimBLEDevice.h>
#include "ESPRandom.h"

#include "ble.h"
#include "gui.h"

Ble * Ble::instance = NULL;

class ServerCallbacks: public NimBLEServerCallbacks {
    void onConnect(NimBLEServer* pServer, ble_gap_conn_desc* desc) {
        NimBLEDevice::startSecurity(desc->conn_handle);
        Serial.print("Client address: ");
        Serial.println(NimBLEAddress(desc->peer_ota_addr).toString().c_str());
        /** We can use the connection handle here to ask for different connection parameters.
         *  Args: connection handle, min connection interval, max connection interval
         *  latency, supervision timeout.
         *  Units; Min/Max Intervals: 1.25 millisecond increments.
         *  Latency: number of intervals allowed to skip.
         *  Timeout: 10 millisecond increments, try for 5x interval time for best results.  
         */
        //pServer->updateConnParams(desc->conn_handle, 24, 48, 0, 60);
    };
    void onDisconnect(NimBLEServer* pServer) {
        Serial.println("Client disconnected");
    };
    void onAuthenticationComplete(ble_gap_conn_desc* desc){
        /** Check that encryption was successful, if not we disconnect the client */  
        if(!desc->sec_state.encrypted) {
            NimBLEDevice::getServer()->disconnect(desc->conn_handle);
            Serial.println("Encrypt connection failed - disconnecting client");
            return;
        }
        Serial.println("Starting BLE work!");
    };
};

class CharacteristicsCallback: public NimBLECharacteristicCallbacks {
    void onWrite(NimBLECharacteristic* pCharacteristic) {
        Ble::instance->setChangedChr(pCharacteristic);
        GUI::instance->render();
        // Logging
        Serial.print(pCharacteristic->getUUID().toString().c_str());
        Serial.print(": onWrite(), value: ");
        Serial.println(pCharacteristic->getValue().c_str());
    };
};

Ble::Ble() {
    instance = this;
}

void Ble::start() {
    NimBLEDevice::init(string(m_name.c_str()));
    //NimBLEDevice::setPower(ESP_PWR_LVL_P9);
    setupSecurity();
    createServer();
}

void Ble::test() {
    start();

    NimBLEServer *pServer = NimBLEDevice::createServer();
    NimBLEService *pService = pServer->createService("ABCD");
    NimBLECharacteristic *pNonSecureCharacteristic = pService->createCharacteristic("1234", NIMBLE_PROPERTY::READ );
    NimBLECharacteristic *pSecureCharacteristic = pService->createCharacteristic("1235", NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::READ_ENC | NIMBLE_PROPERTY::READ_AUTHEN);

    pService->start();
    pNonSecureCharacteristic->setValue("Hello Non Secure BLE");
    pSecureCharacteristic->setValue("Hello Secure BLE");

    NimBLEAdvertising *pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID("ABCD");
    pAdvertising->start();
}

void Ble::bond() {
    NimBLEServer *pServer = NimBLEDevice::getServer();
    pServer->startAdvertising();
}

void Ble::promptPassword() {
    NimBLEServer *pServer = NimBLEDevice::getServer();
    NimBLEService *pService = pServer->createService(passwordPromptServUUID);
    NimBLECharacteristic *pCharacteristic = pService->createCharacteristic(passwordPromptChrUUID, NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::WRITE_ENC | NIMBLE_PROPERTY::WRITE_AUTHEN);
    pCharacteristic->setCallbacks(new CharacteristicsCallback());
    pService->start();

    NimBLEAdvertising *pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(passwordPromptServUUID);
    pAdvertising->start();
}

void Ble::unpromptPassword() {
    NimBLEServer *pServer = NimBLEDevice::getServer();
    pServer->removeService(pServer->getServiceByUUID(passwordPromptServUUID),true);
}

void Ble::setupSecurity() {
    NimBLEDevice::setSecurityAuth(true, true, true);
    NimBLEDevice::setSecurityPasskey(generatePin());
    NimBLEDevice::setSecurityIOCap(BLE_HS_IO_DISPLAY_ONLY);
}

NimBLEServer* Ble::createServer() {
    NimBLEServer *pServer = NimBLEDevice::createServer();
    pServer->setCallbacks(new ServerCallbacks());
    return pServer;
}

int Ble::generatePin() {
    // Need to get entropy
    if (WiFi.getMode() == WIFI_MODE_NULL) {
        WiFi.begin();
    }
    // this is safe, using true random generator esp_random() on backend
    // https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/WMath.cpp
    int pin = random(100000,999999);
    // WiFi is used for entropy for the generation, but is not needed later on
    WiFi.mode(WIFI_OFF);

    return pin;
}

bool Ble::isRunning() {
    return NimBLEDevice::getInitialized();
}

void Ble::stop() {
    if (isRunning()) {
        NimBLEDevice::deinit(true);
    }
}

void Ble::stopAdvertising() {
    NimBLEDevice::getServer()->stopAdvertising();
}

int Ble::getPin() {
    return NimBLEDevice::getSecurityPasskey();
}

void Ble::setChangedChr(NimBLECharacteristic* chr) {
    m_changedChr = chr;
}
NimBLECharacteristic* Ble::getChangedChr() {
    return m_changedChr;
};