using namespace std;
#include <Arduino.h>

#include "WiFi.h"

#include "gui.h"
#include "ArduinoNvs.h"

#define ADC_EN          14
#define ADC_PIN         34

GUI gui = GUI();

void setup()
{
    NVS.begin();
    Serial.begin(115200);
    Serial.println("Start");
    gui.init();
    gui.render();
}

void loop()
{
    gui.tick();
}