#include <Arduino.h>

#include <TFT_eSPI.h>
#include <Button2.h>

#ifndef TFT_DISPOFF
#define TFT_DISPOFF 0x28
#endif

#ifndef TFT_SLPIN
#define TFT_SLPIN   0x10
#endif

#define BUTTON_1        35
#define BUTTON_2        0

#define PHRASE_LEN 24

#include "gui.h"
#include "wallet.h"
#include "ble.h"
#include "utils.h"

#include "bluetooth.png.h"
#include "succ.png.h"
#include "fail.png.h"

GUI * GUI::instance = NULL;

string menuItems[6] PROGMEM = {
    "Sign transaction via BLE",
    "Display public key",
    "Display private key",
    "Pair BLE device",
    "Wipe device (generate new key)",
    "About"
};

GUI::GUI() : tft(135, 240), buttonA(BUTTON_1), buttonB(BUTTON_2), w(), b() {
    updateMenu(menuItems);

    instance = this;

    tft.init();
    tft.fontHeight(2);
    tft.setRotation(1);
    tft.fillScreen(TFT_BLACK);
    tft.setTextColor(TFT_WHITE);
    tft.setTextWrap(true);
    tft.setTextSize(1);
    tft.setSwapBytes(true);

    pinMode(BUTTON_1, INPUT_PULLUP);
    pinMode(BUTTON_2, INPUT_PULLUP);

    buttonA.setClickHandler(handleClickWrapper);
    buttonB.setClickHandler(handleClickWrapper);

    m_selected = 0;
    m_opened = menu;
    m_step = 0;
}

void GUI::init() {
    w.load();
}

void GUI::updateMenu(std::string items[]) {
    m_items = items;
}

void GUI::tick() {
    buttonA.loop();
    buttonB.loop();
}

void GUI::render() {
    if(m_opened!=menu){
        tft.fillScreen(TFT_BLACK);
    }
    switch (m_opened) {
        case menu:
            drawMenu();
            return;
        case signBLE:
            drawSignBLE();
            return;
        case displayPbKey:
            drawDisplayPbKey();
            return;
        case displayPvKey:
            drawDisplayPvKey();
            return;
        case pair:
            drawPair();
            return;
        case generateKey:
            drawGenerateKey();
            return;
        case about:
            drawAbout();
            return;
    };
}

void GUI::drawMenu() {
    for (int i=0; i<6;i++) {
        const char * c = m_items[i].c_str();
        if(m_selected==i) {
            tft.fillRect(0, i*22, 240, i*22+22, TFT_VIOLET);
        } else{
            tft.fillRect(0, i*22, 240, i*22+22, TFT_BLACK);
        }
        tft.drawString(c, 5, i*22+3, 2);
    }
}

void GUI::clickMenu(Button2& btn) {
    if (btn == buttonA) {
        m_step = 0;
        switch (m_selected) {
            case 0:
                m_opened = signBLE;
                break;
            case 1:
                m_opened = displayPbKey;
                break;
            case 2:
                m_opened = displayPvKey;
                break;
            case 3:
                m_opened = pair;
                break;
            case 4:
                m_opened = generateKey;
                break;
            case 5:
                m_opened = about;
                break;
        }
        render();
    } else if (btn == buttonB) {
        m_selected += 1;
        if(m_selected==6){
            m_selected = 0;
        }
        render();
    }
}

void GUI::drawSignBLE() {
    drawHeader("Sign transaction?");
    w.load();
}

void GUI::clickSignBLE(Button2& btn) {
    if (btn == buttonA) {
        // TODO
    } else if (btn == buttonB) {
        m_opened = menu;
        render();
    }
}

void GUI::drawDisplayPbKey() {
    drawHeader("Public key");
    drawSplitString(w.getPhrase()+"; "+(w.checkPassword("test")?"true":"false"));
}

void GUI::clickDisplayPbKey(Button2& btn) {
    if (btn == buttonA) {
        // TODO
    } else if (btn == buttonB) {
        m_opened = menu;
        render();
    }
}

void GUI::drawDisplayPvKey() {
    switch (m_step) {
        case 0:
            drawHeader("Private key");
            drawBLEPrompt("Enter the password");
            b.start();
            b.promptPassword();
            m_step += 1;
            return;
        case 1:
            b.unpromptPassword();
            drawHeader("Password");
            if (b.getChangedChr()->getUUID().equals(b.passwordPromptChrUUID)) {
                const String password = b.getChangedChr()->getValue().c_str();
                if (w.checkPassword(password)) {
                    m_step += 1;
                    w.unlock(password);
                    flashSuccess("Unlocked!");
                } else {
                    b.promptPassword();
                    drawFail("Wrong password");
                }
            } else {
                b.stop();
                m_opened = menu;
                flashFail("BLE error");
            }
            return;
        case 2:
            b.stop();
            drawHeader("Private key");
            //drawSplitString(w.getPrivateKey());
            return;
    }
}

/*switch (m_step) {
        case 0:
            drawHeader("Private key");
            drawBLEPrompt("Enter the password");
            b.start();
            b.promptPassword();
            m_step += 1;
            return;
        case 1:
            drawHeader("Ted se to posralo");
            if (b.getChangedChr()->getUUID().equals(b.passwordPromptChrUUID)) {
                drawFail(b.getChangedChr()->getValue().c_str());
            }
            return;
    }*/

void GUI::clickDisplayPvKey(Button2& btn) {
    if (btn == buttonA) {
        // TODO
    } else if (btn == buttonB) {
        b.stop();
        m_opened = menu;
        render();
    }
}

void GUI::drawPair() {
    drawHeader("Connect to device 'Garlic'");
    b.start();
    b.bond();
    // draw PIN
    tft.drawCentreString("PIN:",tft.width()/2,(tft.height()/2)-30,4);
    tft.setTextSize(2);
    tft.drawCentreString(String(b.getPin()),tft.width()/2,tft.height()/2,4);
    tft.setTextSize(1);
}

void GUI::clickPair(Button2& btn) {
    if (btn == buttonA) {
        // TODO
    } else if (btn == buttonB) {
        // TODO: This doesn't work sometimes. You can't pair again when you exit the page for some reason...
        b.stop();
        m_opened = menu;
        render();
    }
}

void GUI::drawGenerateKey() {
    switch (m_step) {
        case 0:
            drawHeader("Are you sure you wanna do this?");
            drawConfirmButtons();
            drawSplitString("This will WIPE the key stored on the device.",20);
            m_step += 1;
            return;
        case 1:
            drawHeader("Write this down carefully");
            w.wipe();
            w.generatePhrase();
            drawSplitString(w.getPhrase());
            m_step += 1;
            return;
        case 2:
            drawHeader("Time to check!");
            drawSplitString("Let's check the phrase!");
            m_step += 1;
            return;
        /*
            cases 3-26 handled bellow
        */
        case 27:
            drawHeader("Set password");
            drawBLEPrompt("Enter the password");
            b.start();
            b.promptPassword();
            m_step += 1;
            return;
        case 28:
            b.unpromptPassword();
            drawHeader("Is this the password you entered?");
            if (b.getChangedChr()->getUUID().equals(b.passwordPromptChrUUID)) {
                const String password = b.getChangedChr()->getValue().c_str();
                drawSplitString(password,20);
                drawConfirmButtons();
                m_step += 1;
            } else {
                b.stop();
                m_opened = menu;
                flashFail("BLE error");
            }
            return;
        case 29:
            drawHeader("Password set");
            if (!b.getChangedChr()->getValue().empty()) {
                w.setPassword(b.getChangedChr()->getValue().c_str());
                b.stop();
                m_opened = menu;
                w.save();
                flashSuccess("Wallet created!");
            } else {
                b.stop();
                m_opened = menu;
                flashFail("Password empty");
            }
            return;
        default:
            break;
    }
    // handle steps 3-26 used for checking the phrase back
    if(2<m_step && m_step<27) {
        drawHeader("Word #"+String(m_step-2));
        tft.setTextSize(2);
        tft.drawCentreString(w.getPhraseAt(m_step-3),tft.width()/2,tft.height()/2,4);
        tft.setTextSize(1);
        m_step += 1;
    }
}

void GUI::clickGenerateKey(Button2& btn) {
    if (btn == buttonA) {
        // if we have the bluetooth prompt, dont rerender on buttonA click
        if (m_step!=27+1) {
            render();
        }
    } else if (btn == buttonB) {
        b.stop();
        m_opened = menu;
        render();
    }
}

void GUI::drawAbout() {
    drawHeader("About");
    drawSuccess("All good!");
}

void GUI::clickAbout(Button2& btn) {
    if (btn == buttonA) {
        // TODO
    } else if (btn == buttonB) {
        m_opened = menu;
        render();
    }
}

void GUI::handleClickWrapper(Button2& btn) {
    GUI::instance->handleClick(btn);
}

void GUI::handleClick(Button2& btn) {
    switch (m_opened) {
        case menu:
            clickMenu(btn);
            break;
        case signBLE:
            clickSignBLE(btn);
            break;
        case displayPbKey:
            clickDisplayPbKey(btn);
            break;
        case displayPvKey:
            clickDisplayPvKey(btn);
            break;
        case pair:
            clickPair(btn);
            break;
        case generateKey:
            clickGenerateKey(btn);
            break;
        case about:
            clickAbout(btn);
            break;
    }
}

void GUI::drawHeader(String text) {
    tft.fillRect(0, 0, 240, 22, TFT_VIOLET);
    const char * c = text.c_str();
    tft.drawString(c, 5, 3, 2);
    tft.setCursor(0,27,2);
}

void GUI::drawConfirmButtons() {
    tft.fillRect(tft.width()-20,0,tft.width()-20,tft.height()/2, TFT_GREEN);
    tft.fillRect(tft.width()-20,tft.height()/2,tft.width()-20,tft.height(), TFT_RED);
}

void GUI::drawBLEPrompt(const String caption) {
    // Bluetooth logo
    tft.pushImage(100, 33,  33, 50, bluetoothLogo);
    // caption
    tft.drawCentreString(caption,tft.width()/2,90,4);
}

void GUI::drawSuccess(const String caption) {
    // Success logo
    tft.pushImage(90, 33,  50, 50, succLogo);
    // caption
    tft.drawCentreString(caption,tft.width()/2,90,4);
}

void GUI::flashSuccess(const String caption) {
    drawSuccess(caption);
    espDelay(3000);
    render();
}

void GUI::drawFail(const String caption) {
    // Fail logo
    tft.pushImage(90, 33,  50, 50, failLogo);
    // caption
    tft.drawCentreString(caption,tft.width()/2,90,4);
}

void GUI::flashFail(const String caption) {
    drawFail(caption);
    espDelay(3000);
    render();
}

// Reference: https://github.com/Bodmer/TFT_eSPI/issues/558#issuecomment-593147012
void GUI::drawSplitString(const String text) {
  int wordStart = 0;
  int wordEnd = 0;
  while ( (text.indexOf(' ', wordStart) >= 0) && ( wordStart <= text.length())) {
    wordEnd = text.indexOf(' ', wordStart + 1);
    uint16_t len = tft.textWidth(text.substring(wordStart, wordEnd));
    if (tft.getCursorX() + len >= tft.width()) {
      tft.println();
      wordStart++;
    }
    tft.print(text.substring(wordStart, wordEnd));
    wordStart = wordEnd;
  }
}
 
void GUI::drawSplitString(const String text, const int rightOffset) {
  int wordStart = 0;
  int wordEnd = 0;
  while ( (text.indexOf(' ', wordStart) >= 0) && ( wordStart <= text.length())) {
    wordEnd = text.indexOf(' ', wordStart + 1);
    uint16_t len = tft.textWidth(text.substring(wordStart, wordEnd));
    if (tft.getCursorX() + len >= (tft.width()-rightOffset)) {
      tft.println();
      wordStart++;
    }
    tft.print(text.substring(wordStart, wordEnd));
    wordStart = wordEnd;
  }
}
