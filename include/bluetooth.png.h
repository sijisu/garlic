// Generated by   : ImageConverter 565 Online
// Generated from : 157px-Bluetooth.svg.png
// Time generated : Sat, 27 Feb 21 16:52:28 +0100  (Server timezone: CET)
// Image Size     : 33x50 pixels
// Memory usage   : 3300 bytes


#if defined(__AVR__)
    #include <avr/pgmspace.h>
#elif defined(__PIC32MX__)
    #define PROGMEM
#elif defined(__arm__)
    #define PROGMEM
#endif

const unsigned short bluetoothLogo[1650] PROGMEM={
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0010 (16) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x0020 (32) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0030 (48) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x0040 (64) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0050 (80) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000,   // 0x0060 (96) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0070 (112) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0080 (128) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0090 (144) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x00A0 (160) pixels
0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x00B0 (176) pixels
0x09F2, 0x09F2, 0x09F2, 0x2A72, 0x3AB3, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x00C0 (192) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x00D0 (208) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xD6DC, 0x2232, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x00E0 (224) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x00F0 (240) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xC65B, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0100 (256) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0110 (272) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xB5B9, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0120 (288) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0130 (304) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0x9D18, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0140 (320) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0150 (336) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFDF, 0x7C56, 0x09F2, 0x09F2,   // 0x0160 (352) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0170 (368) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xF7BF, 0x6BB5,   // 0x0180 (384) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0190 (400) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,   // 0x01A0 (416) pixels
0xEF7E, 0x4B13, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x01B0 (432) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xCE9B, 0xDEFD,   // 0x01C0 (448) pixels
0xFFFF, 0xFFFF, 0xE71D, 0x3272, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x01D0 (464) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xCE7B,   // 0x01E0 (480) pixels
0x42F3, 0xEF5E, 0xFFFF, 0xFFFF, 0xD6BC, 0x2232, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x01F0 (496) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x1212, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF,   // 0x0200 (512) pixels
0xCE7B, 0x09F2, 0x5B74, 0xF79E, 0xFFFF, 0xFFFF, 0xBE1A, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0210 (528) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0xB5DA, 0xC65B, 0x1A12, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF,   // 0x0220 (544) pixels
0xFFFF, 0xCE7B, 0x09F2, 0x09F2, 0x7C36, 0xFFDF, 0xFFFF, 0xFFFF, 0xAD79, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0230 (560) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0xB5DA, 0xFFFF, 0xFFFF, 0xC65B, 0x1212, 0x09F2, 0x09F2, 0x5334,   // 0x0240 (576) pixels
0xFFFF, 0xFFFF, 0xCE7B, 0x09F2, 0x09F2, 0x09F2, 0x94F7, 0xFFFF, 0xFFFF, 0xFFFF, 0x94D7, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0250 (592) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xC65B, 0x1212, 0x09F2,   // 0x0260 (608) pixels
0x5334, 0xFFFF, 0xFFFF, 0xCE7B, 0x09F2, 0x09F2, 0x3293, 0xDEFC, 0xFFFF, 0xFFFF, 0xF7BF, 0x7415, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0270 (624) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xC65B,   // 0x0280 (640) pixels
0x1212, 0x5334, 0xFFFF, 0xFFFF, 0xCE7B, 0x09F2, 0x3AB3, 0xDEFD, 0xFFFF, 0xFFFF, 0xF7BE, 0x6BF5, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0290 (656) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x8476, 0xFFDF, 0xFFFF,   // 0x02A0 (672) pixels
0xFFFF, 0xC67B, 0x5354, 0xFFFF, 0xFFFF, 0xCE7B, 0x3AB3, 0xDF1D, 0xFFFF, 0xFFFF, 0xF79E, 0x6BD5, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x02B0 (688) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x8476,   // 0x02C0 (704) pixels
0xFFDF, 0xFFFF, 0xFFFF, 0xD6BC, 0xFFFF, 0xFFFF, 0xCEBC, 0xE73D, 0xFFFF, 0xFFFF, 0xF79E, 0x63B4, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x02D0 (720) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x02E0 (736) pixels
0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xEF7E, 0x6394, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x02F0 (752) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0300 (768) pixels
0x09F2, 0x09F2, 0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xEF7E, 0x5B74, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0310 (784) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0320 (800) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xFFFF, 0xEF7E, 0x5354, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0330 (816) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0340 (832) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5354, 0xEF9E, 0xFFFF, 0xFFFF, 0xFFFF, 0xCE9B, 0x1A12, 0x09F2, 0x09F2, 0x09F2,   // 0x0350 (848) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0360 (864) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5B74, 0xEF7E, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xC67B, 0x1212, 0x09F2,   // 0x0370 (880) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0380 (896) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x6394, 0xEF7E, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xC65B,   // 0x0390 (912) pixels
0x1212, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x03A0 (928) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x63B4, 0xEF9E, 0xFFFF, 0xFFFF, 0xEF7E, 0xFFFF, 0xFFFF, 0xE75E, 0xFFDF, 0xFFFF,   // 0x03B0 (944) pixels
0xFFFF, 0xC65B, 0x1A12, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x03C0 (960) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x6BD5, 0xF79E, 0xFFFF, 0xFFFF, 0xDF1D, 0x63B5, 0xFFFF, 0xFFFF, 0xCE7B, 0x8476,   // 0x03D0 (976) pixels
0xFFDF, 0xFFFF, 0xFFFF, 0xC65B, 0x1A12, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x03E0 (992) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x73F5, 0xF7BE, 0xFFFF, 0xFFFF, 0xDEFD, 0x3A93, 0x5334, 0xFFFF, 0xFFFF, 0xCE7B,   // 0x03F0 (1008) pixels
0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xC65B, 0x1212, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0400 (1024) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x7415, 0xF7BF, 0xFFFF, 0xFFFF, 0xDEFC, 0x3293, 0x09F2, 0x5334, 0xFFFF, 0xFFFF,   // 0x0410 (1040) pixels
0xCE7B, 0x09F2, 0x09F2, 0x8496, 0xFFDF, 0xFFFF, 0xFFFF, 0xC65B, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0420 (1056) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0xBE1A, 0xFFFF, 0xFFFF, 0xD6DC, 0x3272, 0x09F2, 0x09F2, 0x5334, 0xFFFF,   // 0x0430 (1072) pixels
0xFFFF, 0xCE7B, 0x09F2, 0x09F2, 0x09F2, 0x8CD7, 0xFFFF, 0xFFFF, 0xFFFF, 0xA579, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0440 (1088) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x11F2, 0xC63B, 0xD6BC, 0x2A52, 0x09F2, 0x09F2, 0x09F2, 0x5334,   // 0x0450 (1104) pixels
0xFFFF, 0xFFFF, 0xCE7B, 0x09F2, 0x09F2, 0x1212, 0xCE9C, 0xFFFF, 0xFFFF, 0xE73D, 0x3293, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0460 (1120) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x1A12, 0x2232, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0470 (1136) pixels
0x5334, 0xFFFF, 0xFFFF, 0xCE7B, 0x09F2, 0x09F2, 0xB5FA, 0xFFFF, 0xFFFF, 0xF79E, 0x5B74, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0480 (1152) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0490 (1168) pixels
0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xCE7B, 0x09F2, 0x9D18, 0xFFFF, 0xFFFF, 0xFFDF, 0x7C36, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x04A0 (1184) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x04B0 (1200) pixels
0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xCE7B, 0x7415, 0xFFDF, 0xFFFF, 0xFFFF, 0x9D38, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x04C0 (1216) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x04D0 (1232) pixels
0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xD6DC, 0xF79E, 0xFFFF, 0xFFFF, 0xBE1A, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x04E0 (1248) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x04F0 (1264) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xD6BC, 0x1A12, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0500 (1280) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0510 (1296) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xE75D, 0x3AB3, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0520 (1312) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0530 (1328) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFFF, 0xF7BE, 0x5B74, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0540 (1344) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0550 (1360) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xFFDF, 0x7C56, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0560 (1376) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0570 (1392) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xFFFF, 0xA558, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0580 (1408) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2,   // 0x0590 (1424) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xFFFF, 0xBE1A, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x05A0 (1440) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2,   // 0x05B0 (1456) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x5334, 0xD6DC, 0x1A32, 0x09F2, 0x09F2, 0x09F2,   // 0x05C0 (1472) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x05D0 (1488) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x2A72, 0x42D3, 0x09F2, 0x09F2, 0x09F2,   // 0x05E0 (1504) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x05F0 (1520) pixels
0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0600 (1536) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x0610 (1552) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0620 (1568) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x0630 (1584) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0640 (1600) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x0650 (1616) pixels
0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2,   // 0x0660 (1632) pixels
0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x09F2, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,   // 0x0670 (1648) pixels
};
