using namespace std;
#include <Arduino.h>
#include <set>
#include "Bitcoin.h"

#ifndef WALLET_H
#define WALLET_H

class Wallet {
private:
    String m_phrase;
    vector<string> m_phraseVector;
    uint8_t m_passwordHash[32];
    HDPrivateKey* m_privatekey;
 
public:
    Wallet();
    void generatePhrase();
    bool phraseGenerated();
    String getPhrase();
    String getPhraseAt(int index);
    void setPassword(String password);
    bool checkPassword(String password);
    void unlock(const String password);
    void lock();
    String getPrivateKey();
    /*
        Storing part
    */
    bool isSaved();
    void save();
    void wipe();
    void load();
};

#endif // WALLET_H