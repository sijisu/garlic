using namespace std;
#include <Arduino.h>

#include <NimBLEDevice.h>

#ifndef BLE_H
#define BLE_H

class Ble {
private:
    const String m_name = "Garlic";
    void setupSecurity();
    int generatePin();
    NimBLEServer* createServer();
    NimBLECharacteristic* m_changedChr;
 
public:
    Ble();
    static Ble *instance;
    void start();
    void stop();
    void stopAdvertising();
    void bond();
    void test();
    bool isRunning();
    int getPin();
    void promptPassword();
    void unpromptPassword();
    void setChangedChr(NimBLECharacteristic* chr);
    NimBLECharacteristic* getChangedChr();

    const NimBLEUUID passwordPromptServUUID = NimBLEUUID("50455765-3347-5a38-594e-324e3233536e");
    const NimBLEUUID passwordPromptChrUUID = NimBLEUUID("586d584f-6f4c-6347-7974-715459776e4b");
};

#endif // BLE_H