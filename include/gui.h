using namespace std;
#include <Arduino.h>

#include <TFT_eSPI.h>
#include <Button2.h>

#include "wallet.h"
#include "ble.h"

#ifndef GUI_H
#define GUI_H

class GUI {
enum View { menu=0, signBLE, displayPbKey, displayPvKey, pair, generateKey, about };
private:
    int m_selected;
    View m_opened;
    int m_step;
    string* m_items;
    TFT_eSPI tft;
    Button2 buttonA;
    Button2 buttonB;
    Wallet w;
    Ble b;
    void updateMenu(string items[]);
    static void handleClickWrapper(Button2& btn);
    void handleClick(Button2& btn);
    void drawMenu();
    void clickMenu(Button2& btn);
    void drawSignBLE();
    void clickSignBLE(Button2& btn);
    void drawDisplayPbKey();
    void clickDisplayPbKey(Button2& btn);
    void drawDisplayPvKey();
    void clickDisplayPvKey(Button2& btn);
    void drawPair();
    void clickPair(Button2& btn);
    void drawGenerateKey();
    void clickGenerateKey(Button2& btn);
    void drawAbout();
    void clickAbout(Button2& btn);
    void drawSplitString(String text);
    void drawSplitString(String text, int rightOffset);
    void drawHeader(String text);
    void drawConfirmButtons();
    void drawBLEPrompt(const String caption);
    void drawSuccess(const String caption);
    void flashSuccess(const String caption);
    void drawFail(const String caption);
    void flashFail(const String caption);
 
public:
    GUI();
    static GUI *instance;
    void init();
    void render();
    void tick();
    void handleBLEWrite(NimBLECharacteristic* chr);
};

#endif // GUI_H